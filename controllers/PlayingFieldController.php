<?php

require_once __DIR__.'/../include/db.php';

$app->get('/', function ($params, $obj) {
	if (isset($params['location_id']) && $params['location_id'] > 0) {
        return getAll("select * from playing_fields where location_id=:location_id", $params);
    }
    return getAll("select * from playing_fields");
});

$app->post('/', function ($params, $obj) {
    return createObject("playing_fields", $obj);
    
});


$app->get('/:id', function ($params, $obj) {
    return getOne("select * from playing_fields where id=:id", $params, $obj);
});


$app->put('/:id', function ($params, $obj) {
    return updateObject("playing_fields", "id=:id", $params, $obj);
});


$app->delete('/:id', function ($params, $obj) {
    return deleteObject("playing_fields", "id=:id", $params, $obj);
});

?>