<?php

require_once __DIR__.'/../include/db.php';

$app->get('/', function ($params, $obj) {
    return getAll("select * from locations");
});

$app->post('/', function ($params, $obj) {
    return createObject("locations", $obj);
    
});


$app->get('/:id', function ($params, $obj) {
    return getOne("select * from locations where id=:id", $params, $obj);
});


$app->put('/:id', function ($params, $obj) {
    return updateObject("locations", "id=:id", $params, $obj);
});


$app->delete('/:id', function ($params, $obj) {
    return deleteObject("locations", "id=:id", $params, $obj);
});

?>